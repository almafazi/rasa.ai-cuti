## Generated Story 3461596384880743218
* greet
    - action_get_time
    - slot{"time": "siang"}
    - utter_greet
* ask_cuti
    - utter_ask_cuti
* tipe_cuti{"tipe_cuti": "tahunan"}
    - slot{"tipe_cuti": "tahunan"}
    - utter_tipe_cuti
* start_date{"start_date": "21 agustus 2018"}
    - slot{"start_date": "21 agustus 2018"}
    - utter_start_date
* lama_cuti{"lama_cuti": "6 hari"}
    - slot{"lama_cuti": "6 hari"}
    - utter_lama_cuti
* alamat{"alamat": "jogja"}
    - slot{"alamat": "jogja"}
    - utter_alamat
* telepon{"telepon": "087776776676"}
    - slot{"telepon": "087776776676"}
    - utter_telepon
* alasan{"alasan": "liburan"}
    - slot{"alasan": "liburan"}
    - utter_alasan
* trip_berangkat{"trip_berangkat": "3 hari"}
    - slot{"trip_berangkat": "3 hari"}
    - utter_trip_berangkat
* ask_trip_pulang{"trip_pulang": "7 hari"}
    - slot{"trip_pulang": "7 hari"}
    - utter_trip_pulang
* affirm
    - utter_konfirmasi
    - utter_greet_finish
* ask_cuti
    - utter_ask_cuti
* tipe_cuti{"tipe_cuti": "perjalanan"}
    - slot{"tipe_cuti": "perjalanan"}
    - utter_tipe_cuti
* start_date{"start_date": "13 november 2018"}
    - slot{"start_date": "13 november 2018"}
    - utter_start_date
* lama_cuti{"lama_cuti": "8 hari"}
    - slot{"lama_cuti": "8 hari"}
    - utter_lama_cuti
* alamat{"alamat": "jogja"}
    - slot{"alamat": "jogja"}
    - utter_alamat
* telepon{"telepon": "089948838774"}
    - slot{"telepon": "089948838774"}
    - utter_telepon
* alasan
    - utter_alasan
* trip_berangkat{"trip_berangkat": "5 hari"}
    - slot{"trip_berangkat": "5 hari"}
    - utter_trip_berangkat
* ask_trip_pulang{"trip_pulang": "8 hari"}
    - slot{"trip_pulang": "8 hari"}
    - utter_trip_pulang
* affirm
    - utter_konfirmasi
    - utter_greet_finish