import requests
import json
import pymongo

from rasa_core_sdk import Action
from rasa_core_sdk.events import SlotSet

class ActionDefaultFallback(Action):
    """Executes the fallback action and goes back to the previous state
    of the dialogue"""

    def name(self):
        return "action_default_fallback";

    def run(self, dispatcher, tracker, domain):
        from rasa_core_sdk.events import UserUtteranceReverted

        dispatcher.utter_template("utter_default", tracker,
                                  silent_fail=True)

        return [UserUtteranceReverted()]
    
class ActionRestart(Action):

    def name(self):
        return "action_default_restart"

    def run(self, dispatcher, tracker, domain):
        from rasa_core_sdk.events import Restarted

        dispatcher.utter_template("utter_greet", tracker,
                                  silent_fail=True)
        return [Restarted()]

class ActionGetTime(Action):
  def name(self):
    return "action_get_time"

  def run(self, dispatcher, tracker, domain):
    request = requests.get('http://68.183.229.24/time').json()
    time = request['time']
    
    return [SlotSet("time", 'siang')]

class ActionApplyCuti(Action):
  def name(self):
    return "action_apply_cuti"

  def run(self, dispatcher, tracker, domain):
      
    myclient = pymongo.MongoClient("mongodb://rasa:example@mongo:27017/")
    mydb = myclient["dbcuti"]
    mycol = mydb["cuti"]
    
    tipe = tracker.get_slot('tipe_cuti')
    start_date = tracker.get_slot('start_date')
    lama_cuti = tracker.get_slot('lama_cuti')
    alamat = tracker.get_slot('alamat')
    telepon = tracker.get_slot('telepon')
    alasan = tracker.get_slot('alasan')
    trip_berangkat = tracker.get_slot('trip_berangkat')
    trip_pulang = tracker.get_slot('trip_pulang')
    
    mydict = { "user": "default", "tipe": tipe, "tanggal_mulai": start_date, "lama_cuti": lama_cuti, "alamat": alamat, "telepon": telepon, "alasan": alasan, "trip_berangkat": trip_berangkat, "trip_pulang": trip_pulang }
    
    mycol.insert_one(mydict)
    return []

class ActionJoke(Action):
  def name(self):
    return "action_joke"

  def run(self, dispatcher, tracker, domain):
    #request = requests.get('http://api.icndb.com/jokes/random').json() #make an api call
    #joke = request['value']['joke'] #extract a joke from returned json response

    data = {"msg" : "24.3"}
    data_json = json.dumps(data)
    headers = {'Content-type': 'application/json'}
    url = 'http://68.183.229.24/test'
    resp = requests.post(url, data=data_json, headers=headers).json()
    cont = resp['msg']
    
    dispatcher.utter_message(cont) #send the message back to the user
    return []